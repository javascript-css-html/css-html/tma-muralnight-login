### Web Design Work Training
## JavaScript CSS HTML

# tma-muralnight-login

Following along with the YouTube video "Pluralsight Login Page Clone - HTML & CSS" by Brad Traversy on his Traversy Media Channel. 

Video link: https://youtu.be/wIx1O5Y5EB4. 

This version of the PLURALSIGHT login page has been named MURIALNIGHT to avoid confusion. Original page: https://www.apple.com/itunes/.

## Badges
![40% HTML](https://img.shields.io/static/v1?label=HTML&message=40%&color=orange)
![60% CSS](https://img.shields.io/static/v1?label=CSS&message=60%&color=blue)
![0% Other](https://img.shields.io/static/v1?label=Other&message=0%&color=lightgray)
![67% Complete](https://img.shields.io/static/v1?label=Completed&message=67%&color=green)

## Current Screenshots

### iPhone Xr

![iPhone Xr](./images/Screen%20Shot%202022-11-21%20iPhone%20Xr%20tma-murialnight-login.png)

### iPad Air

![iPad Air](./images/Screen%20Shot%202022-11-21%20iPad%20Air%20tma-murialnight-login.png)

### Desktop

![Desktop](./images/Screen%20Shot%202022-11-21%20Desktop%20tma-murialnight-login.png)
## Installation
This is a static website with 'css' and 'images' folders. To activate the Snyk Security extension I've installed a 

Other technologies: https://fontawsome.com kit script and https://fonts.google.com/specimen/PT+Sans. These links are both installed in the head element of 'index.html'.

## Usage
Test and show my skills in web design.

## Support and Contributions
Raise an issue or hit me up at bigsteve@redandblackzone.com or message me on GitHub.

## Roadmap
Lets see how close I can get to the real thing.

## Authors and Acknowledgment
This project is based on the YouTube video "Pluralsight Login Page Clone - HTML & CSS" by Brad Traversy (https://github.com/bradtraversy) on his Traversy Media Channel. 

Video link: https://youtu.be/wIx1O5Y5EB4. Codepen: https://codepen.io/bradtraversy/pen/MzyJqw.

## License
No license.
